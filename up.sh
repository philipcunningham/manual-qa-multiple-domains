#!/usr/bin/env bash

set -x

setup() {
  rm output/* >/dev/null 2>&1

  touch output/zap.out

  docker-compose up --detach
}

cleanup() {
  docker-compose down
}
trap cleanup EXIT

setup


docker run \
      --network manual-qa-multiple-domains_test \
      registry.gitlab.com/security-products/dast:3 \
      curl -v http://subdomain.main-site/no-content-type

docker run \
      -v "${PWD}/output":/output \
      --env DAST_WEBSITE=http://main-site \
      --env DAST_DEBUG=true \
      --network manual-qa-multiple-domains_test \
      registry.gitlab.com/security-products/dast:3

jq . output/gl-dast-report.json > output/gl-dast-report.pretty.json
